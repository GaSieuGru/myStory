var conf = require('./conf');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var userApi = require('./api/user');
var registerApi = require('./api/register');
var commentApi = require('./api/comment');
var authApi = require('./api/auth');

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));

//app.use('/', express.static('public'));
app.use(express.static(__dirname + '/public'));

app.post('/register', registerApi);
app.post('/access', authApi.access);
app.use('/api', authApi.authenticate);
app.use('/api/users', userApi);
app.use('/api/comments', commentApi);

app.listen(conf.port, function() {
	console.log('listening on port ' + conf.port);
});
