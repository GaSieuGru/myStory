var db = require('../db');
var router = require('express').Router();

router.get('/', function(req, res) {
	db.comment.find({}, {}, function(error, data) {
		if (error) throw error;
		res.json({
			success: true,
			result: data
		});
	});
});

router.get('/:id', function(req, res) {
	db.comment.findOne({
		_id: req.params.id
	}, function(err, result) {
		if (err) throw err;
		if (result) {
			res.json({
				success: true,
				result: result
			});
		} else {
			res.json({
				success: false,
				message: 'Comment not found'
			});
		}
	});
});

router.post('/', function(req, res) {
	if (!req.body.resource || !req.body.content) {
		res.status(400).json({
			success: false,
			message: 'Mandatory fields (resource and content) must be specified'
		});
		return;
	}
	var comment = {
		resource: req.body.resource,
		content: req.body.content,
		author: req.login.email,
		time: new Date(),
		status: 'public',
		likes: []
	};
	db.comment.insert(comment, function(error, result) {
		if (error) throw error;
		res.json({
			success: true,
			result: result
		});
	});
});

//router.put('/:id', function(req, res) {
//	db.user.findOne({
//		email: req.params.id
//	}, function(error, user) {
//		if (error) throw error;
//		if (!user) {
//			res.json({
//				success: false,
//				message: 'User not found'
//			});
//		} else {
//			var newName = req.body.newName;
//			var newPassword = req.body.newPassword;
//			if (newPassword) {
//				if (!req.body.currentPassword) {
//					res.status(400);
//					res.json({
//						success: false,
//						message: 'Current password must be specified'
//					});
//					return;
//				}
//				if (user.password !== req.body.currentPassword) {
//					res.status(400);
//					res.json({
//						success: false,
//						message: 'Current password does not match'
//					});
//					return;
//				}
//			}
//			var set = {};
//			if (newName) {
//				set.name = newName;
//			}
//			if (newPassword) {
//				set.password = newPassword;
//			}
//			db.user.update(user, {
//				$set: set
//			}, {}, function(err, result) {
//				if (err) throw err;
//				res.json({
//					success: true,
//					message: 'Updated ' + result + ' record(s)'
//				});
//			});
//		}
//	});
//});
//
//router.delete('/:id', function(req, res) {
//	if (req.login.type !== "admin" && req.login.email !== req.params.id) {
//		res.json({
//			success: false,
//			message: 'Not enough privilege to delete another user'
//		});
//	} else {
//		db.user.remove({
//			email: req.params.id
//		}, {
//			multi: false
//		}, function(error, result) {
//			if (error) throw error;
//			if (result === 0) {
//				res.json({
//					success: false,
//					message: 'User not found'
//				});
//			} else {
//				res.json({
//					success: true,
//					message: 'Deleted succesfully'
//				});
//			}
//		});
//	}
//});

module.exports = router;