var db = require('../db');

var register = function(req, res) {
	if (!req.body.name || !req.body.email || !req.body.password) {
		res.status(400).json({
			success: false,
			message: 'Mandatory fields (name, email and password) must be specified'
		});
		return;
	}
	db.user.find({}, function(error, results) {
		var found = false;
		for (var i = 0, n = results.length; i < n; i++) {
			if (results[i].email === req.body.email) {
				found = true;
				break;
			}
		}
		if (found) {
			res.status(400).json({
				success: false,
				message: 'Email has already been registered'
			});
			return;
		}
		var user = {
			name: req.body.name,
			email: req.body.email,
			password: req.body.password,
			type: 'member'
		};
		db.user.insert(user, function(error, result) {
			if (error) throw error;
			result.password = undefined; // hide stored password from client side
			res.json(result);
		});
	});
};

module.exports = register;
