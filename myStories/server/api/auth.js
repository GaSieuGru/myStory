var jwt = require('jsonwebtoken');
var db = require('../db');
var conf = require('../conf');

module.exports = {

	access: function(req, res) {
		if (!req.body.email) {
			res.json({
				success: false,
				message: 'Email must be provided'
			});
			return;
		}
		if (!req.body.password) {
			res.json({
				success: false,
				message: 'Password must be provided'
			});
			return;
		}
		db.user.findOne({
			email: req.body.email
		}, function(err, user) {
			if (err) throw err;
			if (!user) {
				res.json({
					success: false,
					message: 'No account found with that email address'
				});
			} else if (user) {
				// check if password matches
				if (user.password !== req.body.password) {
					res.json({
						success: false,
						message: 'Password is incorrect'
					});
				} else {
					// if user is found and password is right
					// create a token that will expire in 24 hours
					var token = jwt.sign(user, conf.tokenSecret, {
						expiresIn: 1440
					});
					// return the information including token as
					// JSON
					res.json({
						success: true,
						message: 'Enjoy your token!',
						token: token
					});
				}
			}
		});
	},

	authenticate: function(req, res, next) {
		// check header or URL parameters or POST parameters for token
		var token = req.body.token || req.query.token || req.headers['x-access-token'];

		if (token) { // verifies secret and checks expiration
			jwt.verify(token, conf.tokenSecret, function(err, decoded) {
				if (err) {
					return res.json({
						success: false,
						message: 'Access token is invalid or expired'
					});
				} else {
					// if everything is good, save to request for use in other routes
					req.login = decoded;
					next();
				}
			});
		} else {
			return res.status(403).send({
				success: false,
				message: 'Access token must be provided'
			});
		}
	}
};
