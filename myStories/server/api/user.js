var db = require('../db');
var router = require('express').Router();

router.get('/', function(req, res) {
	db.user.find({}, {
		password: 0
	}, function(error, data) {
		if (error) throw error;
		res.json({
			success: true,
			result: data
		});
	});
});

router.get('/:id', function(req, res) {
	db.user.findOne({
		email: req.params.id
	}, function(err, user) {
		if (user) {
			// check whether to respond password or not
			if (req.query.showPassword !== 'true') {
				user.password = undefined;
			}
			res.json({
				success: true,
				result: user
			});
		} else {
			res.json({
				success: false,
				message: 'User not found'
			});
		}
	});
});

router.put('/:id', function(req, res) {
	db.user.findOne({
		email: req.params.id
	}, function(error, user) {
		if (error) throw error;
		if (!user) {
			res.json({
				success: false,
				message: 'User not found'
			});
		} else {
			var newName = req.body.newName;
			var newPassword = req.body.newPassword;
			if (newPassword) {
				if (!req.body.currentPassword) {
					res.status(400);
					res.json({
						success: false,
						message: 'Current password must be specified'
					});
					return;
				}
				if (user.password !== req.body.currentPassword) {
					res.status(400);
					res.json({
						success: false,
						message: 'Current password does not match'
					});
					return;
				}
			}
			var set = {};
			if (newName) {
				set.name = newName;
			}
			if (newPassword) {
				set.password = newPassword;
			}
			db.user.update(user, {
				$set: set
			}, {}, function(err, result) {
				if (err) throw err;
				res.json({
					success: true,
					message: 'Updated ' + result + ' record(s)'
				});
			});
		}
	});
});

router.delete('/:id', function(req, res) {
	if (req.login.type !== "admin" && req.login.email !== req.params.id) {
		res.json({
			success: false,
			message: 'Not enough privilege to delete another user'
		});
	} else {
		db.user.remove({
			email: req.params.id
		}, {
			multi: false
		}, function(error, result) {
			if (error) throw error;
			if (result === 0) {
				res.json({
					success: false,
					message: 'User not found'
				});
			} else {
				res.json({
					success: true,
					message: 'Deleted succesfully'
				});
			}
		});
	}
});

module.exports = router;