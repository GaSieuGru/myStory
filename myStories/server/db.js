var Datastore = require('nedb');
var db = {};

db.item = new Datastore({
	filename: "data/item.db",
	autoload: true
});

db.user = new Datastore({
	filename: "data/user.db",
	autoload: true
});

db.comment = new Datastore({
	filename: "data/comment.db",
	autoload: true
});

module.exports = db;