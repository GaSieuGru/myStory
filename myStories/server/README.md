# Server - myStories

> Manage your personal projects easily

## Build Setup

``` bash
# install dependencies
npm install

# start server at localhost:3000 without debugging
npm start

# start server at localhost:3000 debugging enabled
npm run debug
```
