import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import Home from './component/home.vue'
import Project from './component/project.vue'
import Board from './component/board.vue'

import normalizeCss from './assets/css/normalize.css'
import baseCss from './assets/css/base.css'
import overlayCss from './assets/css/overlay.css'
import tagCss from './assets/css/tag.css'
import inputCss from './assets/css/input.css'

Vue.use(VueRouter)
Vue.use(VueResource)

var router = new VueRouter({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    }, {
      path: '/project',
      name: 'project',
      component: Project
    }, {
      path: '/board',
      name: 'board',
      component: Board
    },
    {
      path: '*',
      component: Home
    }
  ]
})
router.data = {
  projects: [{
    id: 1,
    name: "Hello World",
    members: [{
      ref: 1,
      role: "owner"
    }, {
      ref: 2,
      role: "member"
    }],
    status_groups: ["New", "Doing", "Done"],
    creation_date: 1499759519660,
    last_update_date: 1499759519660
  },
  {
    id: 2,
    name: "Todo Daily",
    members: [{
      ref: 1,
      role: "owner"
    }],
    status_groups: ["New", "Doing", "Done"],
    creation_date: 1499759519660,
    last_update_date: 1499759519660
  }]
}
const app = new Vue({
  router
}).$mount('#app')
