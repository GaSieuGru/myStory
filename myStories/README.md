# myStories

This app helps managing projects easier based on concepts of Agile Kanban Board.

# Design Idea

## Rationale

A guest will register to become member.

A member can create his own project and invite other members he's known to join, or he can be invited to participate in a project created by another member.

A member will play a role, which limits his privileges, when invited into a project. See table below:

||Owner|Member|
|---|---|---|
|Create/update/delete project|v||
|Invite/remove member into/from project|v||
|Transfer ownership for a member|v||
|Create/update/delete group|v||
|Create/update/delete item|v|v|
|Assign item to member|v|v|
|Sign up item|v|v|
|Move item into group|v|v|

An administrator has management interface which is different than that of members, and allows him to manage all registered members and their created data, also as manages global usage rules.

## Basic Use Case

- A guest registers to become myStories's member.
- The guest logins (the member).
- The member creates a project profile.
- The member opens the project profile.
- The member sets/creates groups.
- The member creates items.
- The member invites other members into the project.
- The member signs up or be assigned the items.
- The members together complete their items and move them into corresponding status groups.

## Components

### User

#### Guest

- Can view app introduction
- Can register to become member

#### Member

- Can login
- Can view and update his member profile: name, bio, password
- Can view his projects and corresponding roles
- Can create new project
- Can be invited into a project

#### Administrator

- Created by directly modifying database
- Have different UI to manage members, created data, rules

## Story (To-do Item)

A story can have below information fields:

- ID
- Title (*)
- Description
- Priority: Low, Medium, High
- Planned Effort (days)
- Actual Effort (days)
- Conversations
- Attachments

An story when created will stay in New group. It then can be moved into other status groups such as Doing, Done or can be moved back to New group.

## Status Group

- Create new story with fields: title, content, type, estimation, priority, ...
- Plan a story into a sprint

